package ru.vpavlova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vpavlova.tm.configuration.ClientConfiguration;
import ru.vpavlova.tm.marker.IntegrationCategory;

public class AdminEndpointTest {

    @NotNull
    AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ClientConfiguration.class);

    private final SessionEndpoint sessionEndpoint = context.getBean(SessionEndpoint.class);

    private final AdminEndpoint adminEndpoint = context.getBean(AdminEndpoint.class);

    private final UserEndpoint userEndpoint = context.getBean(UserEndpoint.class);

    @Nullable
    private Session sessionAdmin;

    @Before
    @SneakyThrows
    public void before() {
        sessionAdmin = sessionEndpoint.openSession("admin", "admin");
        ;
    }

    @After
    @SneakyThrows
    public void after() {
        sessionEndpoint.closeSession(sessionAdmin);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeUserByLoginTest() {
        adminEndpoint.removeOneByLogin(sessionAdmin, "user");
    }

    @Test
    @Category(IntegrationCategory.class)
    public void lockUserByLoginTest() {
        adminEndpoint.lockUserByLogin(sessionAdmin, "user");
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void unlockUserByLoginTest() {
        @NotNull final Session session = sessionEndpoint.openSession("user", "user");
        adminEndpoint.unlockUserByLogin(session, "user");
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void createUserTest() {
        adminEndpoint.createUser(sessionAdmin, "login1", "password1");
        Assert.assertNotNull(userEndpoint.findUserByLogin(sessionAdmin, "test1"));
    }

}
