package ru.vpavlova.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.event.ConsoleEvent;
import ru.vpavlova.tm.listener.AbstractListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class FileScanner implements Runnable {

    private static final int INTERVAL = 3;

    @NotNull
    private static final String PATH = "./";

    @Nullable
    @Autowired
    private ApplicationEventPublisher publisher;

    @Nullable
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    private final Collection<String> commands = new ArrayList<>();

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public void init() {
        for (@NotNull final AbstractListener command : listeners) {
            if (command.arg() != null) commands.add(command.name());
        }
        es.scheduleWithFixedDelay(this, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void run() {
        @NotNull final File file = new File(PATH);
        for (File item : file.listFiles()) {
            if (!item.isFile()) continue;
            @NotNull final String fileName = item.getName();
            final boolean chek = commands.contains(fileName);
            if (!chek) continue;
            publisher.publishEvent(new ConsoleEvent(fileName));
            System.out.println();
            item.delete();
        }
    }

}
