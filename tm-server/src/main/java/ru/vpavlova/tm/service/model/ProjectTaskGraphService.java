package ru.vpavlova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vpavlova.tm.repository.model.IProjectGraphRepository;
import ru.vpavlova.tm.repository.model.ITaskGraphRepository;
import ru.vpavlova.tm.api.service.model.IProjectTaskGraphService;
import ru.vpavlova.tm.entity.TaskGraph;
import ru.vpavlova.tm.exception.empty.EmptyIdException;

import java.util.List;

@Service
public final class ProjectTaskGraphService implements IProjectTaskGraphService {

    @NotNull
    @Autowired
    public ITaskGraphRepository taskGraphRepository;

    @NotNull
    @Autowired
    public IProjectGraphRepository projectGraphRepository;

    @NotNull
    public ITaskGraphRepository getTaskRepository() {
        return taskGraphRepository;
    }

    @NotNull
    public IProjectGraphRepository getProjectRepository() {
        return projectGraphRepository;
    }

    @NotNull
    @Override
    public List<TaskGraph> findAllTaskByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    @SneakyThrows
    public void bindTaskByProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        taskRepository.bindTaskByProjectId(userId, projectId, taskId);
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        taskRepository.unbindTaskFromProjectId(userId, taskId);
    }

    @Override
    @SneakyThrows
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        @NotNull final IProjectGraphRepository projectRepository = getProjectRepository();
        taskRepository.removeAllByProjectId(userId, projectId);
        projectRepository.removeOneByIdAndUserId(userId, projectId);
    }

}
