package ru.vpavlova.tm.repository;

import ru.vpavlova.tm.model.Project;

import java.util.*;

public class ProjectRepository {

    private Map<String, Project> projects = new LinkedHashMap<>();

    {
        create();
        create();
        create();
    }

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    public void create() {
        Project project = new Project("project", "new");
        projects.put(project.getId(), project);
    }

    public void removeById(final String id) {
        projects.remove(id);
    }

    public List<Project> findAll() {
        return new ArrayList<>(projects.values()) ;
    }

    public Project findById(final String id) {
        return projects.get(id);
    }

    public void save(final Project project) {
        projects.put(project.getId(), project);
    }

}
