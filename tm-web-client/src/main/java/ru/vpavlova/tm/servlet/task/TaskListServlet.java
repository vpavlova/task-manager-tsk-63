package ru.vpavlova.tm.servlet.task;

import ru.vpavlova.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/tasks/*")
public class TaskListServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("view_name", "TASKS");
        req.setAttribute("tasks", TaskRepository.getInstance().findAll());
        req.getRequestDispatcher("/WEB-INF/views/task-list.jsp").forward(req, resp);
    }

}