package ru.vpavlova.tm.model;

import lombok.*;
import ru.vpavlova.tm.enumerated.Status;

import java.util.*;

@Getter
@Setter
public abstract class AbstractBusinessEntity {

    protected String id = UUID.randomUUID().toString();

    protected Date created = new Date();

    protected Date dateFinish;

    protected Date dateStart;

    protected String description = "";

    protected String name = "";

    protected Status status = Status.NOT_STARTED;

    private String userId;

}
