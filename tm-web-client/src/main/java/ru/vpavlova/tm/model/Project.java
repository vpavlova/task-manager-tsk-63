package ru.vpavlova.tm.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
public class Project extends AbstractBusinessEntity {

    public Project(String name,final String description) {
        this.name = name;
        this.description = description;
    }

}
