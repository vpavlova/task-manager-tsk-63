<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>
<form action="/task/edit/?id=${task.id}" method="POST">
    <input type="hidden" name="id" value="${task.id}"/>
    <table>
        <tr>
            <td>Name:</td>
            <td>Status:</td>
            <td>Description:</td>
        </tr>
        <tr>
            <td>
                <input type="text" name="name" value="${task.name}"/>
            </td>
            <td>
                <select name="status">
                    <c:forEach var="st" items="${enumStatus}">
                        <c:choose>
                            <c:when test="${task.status.name() == st.name()}">
                                <option selected>${st.name()}</option>
                            </c:when>
                            <c:otherwise>
                                <option>${st.name()}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </td>
            <td>
                <input type="text" name="description" value="${task.description}"/>
            </td>
        </tr>
    </table>
    <button type="submit">Submit</button>
</form>
<jsp:include page="../include/_footer.jsp" />