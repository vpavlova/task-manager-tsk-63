<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>
<form action="/project/edit/?id=${project.id}" method="POST">
    <input type="hidden" name="id" value="${project.id}"/>
    <table>
        <tr>
            <td>Name:</td>
            <td>Status:</td>
            <td>Description:</td>
        </tr>
        <tr>
            <td>
                <input type="text" name="name" value="${project.name}"/>
            </td>
            <td>
                <select name="status">
                    <c:forEach var="st" items="${enumStatus}">
                        <c:choose>
                            <c:when test="${project.status.getDisplayName() == st.getDisplayName()}">
                                <option selected>${st.getDisplayName()}</option>
                            </c:when>
                            <c:otherwise>
                                <option>${st.getDisplayName()}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </td>
            <td>
                <input type="text" name="description" value="${project.description}"/>
            </td>
        </tr>
    </table>
    <button type="submit">Submit</button>
</form>
<jsp:include page="../include/_footer.jsp" />