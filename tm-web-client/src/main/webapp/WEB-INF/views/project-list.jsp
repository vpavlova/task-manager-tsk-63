<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Status</th>
        <th>Description</th>
        <th class="mini">Edit</th>
        <th class="mini">Delete</th>
    </tr>
    <c:forEach var="project" items="${projects}">
        <tr>
            <td>
                <c:out value="${project.id}"/>
            </td>
            <td>
                <c:out value="${project.name}"/>
            </td>
            <td>
                <c:out value="${project.status.getDisplayName()}"/>
            </td>
            <td>
                <c:out value="${project.description}"/>
            </td>
            <td class="mini">
                <a href="/project/edit/?id=${project.id}">Edit</a>
            </td>
            <td class="mini">
                <a href="/project/delete/?id=${project.id}">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>
<form action="/project/create">
    <button type="submit">Create</button>
</form>
<jsp:include page="../include/_footer.jsp" />